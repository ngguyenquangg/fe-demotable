import React from 'react';
import Header from '../UIcomponents/viewHome/Header';
import NavBar from '../UIcomponents/viewHome/NavBar';
import Collection from '../UIcomponents/viewHome/Collection';
import Products from '../pages/auctioneer/auction/ProductsAuctionComponent';
import HotDeal from '../UIcomponents/viewHome/HotDeal';
import NewLetter from '../UIcomponents/viewHome/NewLetter';
import Footer from '../UIcomponents/viewHome/Footer';


function Home(props) {
    return (
        <div>
            <Header/>
            <NavBar/>
            {/* <Collection/> */}
            <Products/>
            {/* <HotDeal/> */}
            <NewLetter/>
            <Footer/>
        </div>
    );
}

export default Home;