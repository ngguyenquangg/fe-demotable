import React from 'react';
import './index.css';
import ManageGoodsReceiptComponents from '../../pages/admin/inventory';
import ManageInventoryComponents from '../../pages/admin/inventory';

function InventoryManagement(props) {
    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-md-12 col-sm-12">
                    <div className="card">
                        <div className="card-header">
                            <h3><i className="fa fa-tag" aria-hidden="true" /> Inventory</h3>
                        </div>
                        <div className="card-body">

                            <ManageInventoryComponents />

                        </div>
                    </div>
                </div>
            </div>
        </div>


    );
}

export default InventoryManagement;