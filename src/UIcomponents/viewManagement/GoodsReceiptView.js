import React from 'react';
import './index.css';
import ManageGoodsReceiptComponents from '../../pages/admin/goodsreceipt';

function GoodsReceiptManagement(props) {
    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-md-12 col-sm-12">
                    <div className="card">
                        <div className="card-header">
                            <h3><i className="fa fa-tag" aria-hidden="true" /> Goods Receipt</h3>
                        </div>
                        <div className="card-body">

                            <ManageGoodsReceiptComponents />

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default GoodsReceiptManagement;