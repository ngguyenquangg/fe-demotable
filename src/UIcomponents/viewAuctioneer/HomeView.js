import React from 'react';
import ProductsAuctionView from '../../pages/auctioneer/auction/ProductsAuctionComponent';

function HomeView(props) {
    return (
        <div className="section">
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <div className="section-title">
                            <h3 className="title">All Products</h3>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <ProductsAuctionView />
                    </div>
                </div>
            </div>
        </div>
    );
}

export default HomeView;