import React, { useState, useEffect } from 'react';
import { API } from '../../setting';
import axios from 'axios';

const AuctionView = (props) => {
    const [data, setData] = useState();
    const ids = window.location.href.split("/")
    const fetchData = () => {
        axios.get(API + "/api/productAuctions/" + ids[ids.length - 1]).then((res) => {
            console.log(res.data)
            setData(res.data)
        });
    };
    useEffect(() => {
        fetchData();
    }, []);
    const { item } = props
    return (
        <div className="section">
            {/* container */}
            <div className="container">
                {/* row */}
                <div className="row">
                    {/* Product main img */}
                    <div className="col-md-6 col-md-push-1">
                        <div id="product-main-img">
                            <div className="product-preview">
                                <img src={data?.product?.image} alt="" />
                            </div>
                        </div>
                    </div>
                    {/* /Product main img */}

                    {/* Product details */}
                    <div className="col-md-5 col-md-push-2">
                        <div className="product-details">
                            <h2 className="product-name">{data?.product?.productName}</h2>
                            <div><a className="review-link" href="#">10 Review(s) | Add your review</a>
                            </div>
                            <div>
                                <br />
                                <h4>Start day: <span>{data?.startDate}</span></h4>
                                <h4>End day: <span>{data?.endDate}</span></h4>
                            </div>
                            <div>
                                <p><b>Min price: </b> ${data?.minPrice} </p>
                                <p><b>Highest current price: </b> <span style={{ fontSize: "40px", color: "red" }}>${data?.incremenent}</span> </p>
                                <p>{data?.description}</p>
                                <span>Auction Price: </span><input type="number" id="quantity" name="quantity" min={1} />
                                <br />
                                <br />
                                <br />
                            </div>

                            <div className="add-to-cart">
                                <button className="add-to-cart-btn"><i className="fa fa-shopping-cart" /> Auction</button>
                            </div>

                            <ul className="product-links">
                                <li>Category:</li>
                                <li><a href="#">{data?.product?.category?.categoryName}</a></li>
                            </ul>
                            <ul className="product-links">
                                <li>Share:</li>
                                <li><a href="#"><i className="fa fa-facebook" /></a></li>
                                <li><a href="#"><i className="fa fa-twitter" /></a></li>
                                <li><a href="#"><i className="fa fa-google-plus" /></a></li>
                                <li><a href="#"><i className="fa fa-envelope" /></a></li>
                            </ul>
                        </div>
                    </div>
                    {/* /Product details */}
                    {/* Product tab */}
                    <div className="col-md-12">
                        <table>
                            <thead>
                                <th>User</th>
                                <th>Auction Price</th>
                                <th>Date</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>User 1</td>
                                    <td>$12</td>
                                    <td>07-07-2020</td>
                                </tr>
                                <tr>
                                    <td>User 2</td>
                                    <td>$12</td>
                                    <td>07-07-2020</td>
                                </tr>
                                <tr>
                                    <td>User 3</td>
                                    <td>$12</td>
                                    <td>07-07-2020</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    {/* /product tab */}
                </div>
                {/* /row */}
            </div>
            {/* /container */}
        </div>
    );
}

export default AuctionView;
