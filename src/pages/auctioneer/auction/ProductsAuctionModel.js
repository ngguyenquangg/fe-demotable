import React from 'react';
import { useHistory } from "react-router-dom";

const ProductsAuctionModel = (props) => {
    const { item } = props
    let history = useHistory();
    function handleClick() {
        history.push("/auction/" + item.id);
      }
    return (
        <div className="products-tabs col-md-4">
            <div className="product">
                <div className="product-img">
                    <img src={item?.image} alt="" />
                    <div className="product-label">
                        <span className="new">NEW</span>
                    </div>
                </div>
                <div className="product-body">
                    <p className="product-category">{item?.categoryName}</p>
                    <h3 className="product-name"><a href="#">{item?.productName}</a></h3>
                    <h4 className="produ0ct-price"><span>Start with --</span>$ {item?.minPrice}</h4>
                </div>
                <div className="add-to-cart">
                    <button className="add-to-cart-btn" onClick={() => handleClick()}><i className="fa fa-shopping-cart" /> Aution now!</button>
                </div>
            </div>
        </div>
    );
}

export default ProductsAuctionModel;
