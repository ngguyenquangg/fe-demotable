import React from 'react';

const ProductModel = (props) => {
    return (
        <div className="products-tabs col-md-4">
            <div className="product">
                <div className="product-img">
                    <img src="./img/product07.png" alt="" />
                    <div className="product-label">
                        <span className="new">NEW</span>
                    </div>
                </div>
                <div className="product-body">
                    <p className="product-category">SmartPhone</p>
                    <h3 className="product-name"><a href="#">Iphone 11 pro max</a></h3>
                    <h4 className="product-price"><span>Start with --</span>$1</h4>
                    <div className="product-rating">
                        <i className="fa fa-star" />
                        <i className="fa fa-star" />
                        <i className="fa fa-star" />
                        <i className="fa fa-star" />
                        <i className="fa fa-star-o" />
                    </div>
                </div>
                <div className="add-to-cart">
                    <button className="add-to-cart-btn"><i className="fa fa-shopping-cart" /> Aution now!</button>
                </div>
            </div>
        </div>
    );
}

export default ProductModel;
