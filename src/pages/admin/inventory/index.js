import React, { useState, useEffect } from "react";
import { Table } from "antd";
import axios from "axios";
import moment from "moment";
import { API } from "../../../setting";

const ManageInventoryComponents = (props) => {
  const [data, setData] = useState([]);
  const fetchData = () => {
    axios.get(API + "/api/inventories").then((res) => {
      console.log(res.data);
      setData(
        res.data.map((ele, index) => {
          return {
            ...ele,
            key: index,
            productName: ele.product.productName,
          };
        })
      );
    });
  };
  useEffect(() => {
    fetchData();
  }, []);
  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
    },
    getCheckboxProps: (record) => ({
      disabled: record.name === "Disabled User", // Column configuration not to be checked
      name: record.name,
    }),
  };
  const columns = [
    {
      title: "ID",
      dataIndex: "id",
    },
    {
      title: "Name Product",
      dataIndex: "productName",
      width: "450px",
    },
    {
      title: "Quantity",
      dataIndex: "quantity",
    },
    {
      title: "createdAt",
      dataIndex: "createdAt",
      render: (text) => moment(text).format("DD MM YYYY hh:mm:ss a"),
    },
  ];
  return (
    <div>
      <Table
        rowSelection={{
          type: "radio",
          ...rowSelection,
        }}
        columns={columns}
        dataSource={data}
      />
    </div>
  );
};

export default ManageInventoryComponents;
